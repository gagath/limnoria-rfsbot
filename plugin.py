###
# Copyright (c) 2024, Agathe Porte
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

import re

from supybot import utils, plugins, ircutils, callbacks
from supybot.commands import *
from supybot.i18n import PluginInternationalization
import supybot.ircmsgs as ircmsgs


_ = PluginInternationalization('Rfsbot')

RFS_MATCH = re.compile(r"^(.*) RFS \[(.*)\]\s*$")


def parse_topic(topic):
    match = RFS_MATCH.match(topic)
    if not match:
        raise ValueError

    left = match.group(1)
    current_packages = match.group(2).split(",")
    current_packages = [p.strip() for p in current_packages if p != ""]

    return (left, current_packages)


def check_can_change_topic(irc, channel):
    c = irc.state.channels[channel]
    if 't' in c.modes and not c.isHalfopPlus(irc.nick):
        irc.error("I can't change the topic, I'm not (half)opped "
                  f"and {channel} is +t.", Raise=True)


class Rfsbot(callbacks.Plugin):
    """Handle RFS requests in /topic"""


    def _set_topic(self, irc, channel, topic):
        check_can_change_topic(irc, channel)
        irc.sendMsg(ircmsgs.topic(channel, topic))


    @wrap(['onlyInChannel'])
    def rfsinit(self, irc, msg, args, channel):
        """
        Inits the topic of the channel for RFS operations.
        """

        topic = irc.state.channels[channel].topic
        try:
            _, _ = parse_topic(topic)
        except ValueError:
            self._set_topic(irc, channel, f"{topic} | RFS []")
            irc.replySuccess()
        else:
            irc.error("Topic is already set up.", Raise=True)


    @wrap(['onlyInChannel', many('anything')])
    def rfs(self, irc, msg, args, channel, packages):
        """<srcpkg1> [<srcpkg2> ...]

        Marks or unmarks the provided srcpkgs as needing to be sponsored in
        the channel's topic.
        """

        topic = irc.state.channels[channel].topic
        try:
            left, current_packages = parse_topic(topic)
        except ValueError:
            irc.error("Could not parse topic, use !rfsinit if no RFS list is"
                      "present.", Raise=True)

        added = []
        removed = []
        for p in packages:
            if p in current_packages:
                current_packages.remove(p)
                removed.append(p)
            else:
                current_packages.append(p)
                added.append(p)

        plist = ", ".join(current_packages)
        new_topic = f"{left} RFS [{plist}]"
        self._set_topic(irc, channel, new_topic)

        reply = []
        if added:
            plist = ", ".join(added)
            reply.append(f"added {plist}")
        if removed:
            plist = ", ".join(removed)
            reply.append(f"removed {plist}")
            # [added ...;] removed ... from RFS topic
            reply = "; ".join(reply) + " from RFS list in topic."
        else:
            # added ... to RFS topic
            reply = "; ".join(reply) + " to RFS list in topic."

        irc.reply(reply)


Class = Rfsbot


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
